module.exports = class DAO {
    /**
     * @callback AccessLayerGetDatas
     * @param {Object{}} rows une liste de donnée issue de la base de donnée 
     */
    /**
     * @callback AccessLayerGetData
     * @param {Object {}} rows une donnée issue de la base de donnée
     */
    /**
     * @callback AccessLayerCUD
     * @param {any} error message d'erreur ou undefined si tout c'est bien passé
     */
    
    // importSql(){
    //         var mysql = require('mysql');
    //         var con = mysql.createConnection({host: "localhost",
    //         user: "phpmyadmin",
    //         password: "root",
    //         database: "bdd"
    // })
    // return con;
    // }
    
    // CRUD USER - CRUD USER - CRUD USER - CRUD USER
    /**
     * 
     * @param {AccessLayerGetDatas} callback
     */
    getUsers(callback) {

        var mysql = require('mysql');
            var con = mysql.createConnection({host: "localhost",
            user: "phpmyadmin",
            password: "root",
            database: "bdd"
        })

        con.query("SELECT * FROM user", function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
           con.end();

    }

    /**
     * @param {Number} id l'id de l'utilisateur selectionne
     * @param {AccessLayerGetData} callback 
     */

    getUserById(id, callback) {
       
        var mysql = require('mysql');
        var con = mysql.createConnection({host: "localhost",
        user: "phpmyadmin",
        password: "root",
        database: "bdd"
    })
      
            con.query("SELECT * FROM user WHERE id=" + id, 
            function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
            con.end();
    }

    /**
     * @param {any} email l'id de l'utilisateur selectionne
     * @param {AccessLayerGetData} callback 
     */
    getUserByEmail(email, callback) {
       
        var mysql = require('mysql');
        var con = mysql.createConnection({host: "localhost",
        user: "phpmyadmin",
        password: "root",
        database: "bdd"
    })
        
            //var result = [];
            con.query("SELECT * FROM user WHERE email='"+email+"'", 
            function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });

            con.end();
    }

    /**
     * @param {any} e données au format json à inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    createUser(e, callback) {
        
        var mysql = require('mysql');
        var con = mysql.createConnection({host: "localhost",
        user: "phpmyadmin",
        password: "root",
        database: "bdd"
    });

            con.query("INSERT INTO user(username,password,email) VALUES('" + e.username + "','" + e.password + "','" + e.email + "')", (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK CREATE");
                }
            });

        con.end();
    }

    /**
     * @param {any} e données au format json a inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    updateUser(e, callback) {

        var mysql = require('mysql');
            var con = mysql.createConnection({host: "localhost",
            user: "phpmyadmin",
            password: "root",
            database: "bdd"
        })
       
            con.query("UPDATE user SET username='" + e.username + "',password='" + e.password + "',email='" + e.email + "' WHERE id=" + e.id, (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK ");
                }
            });

        con.end();
    }

    
    /** 
     * @param {number} id //id de l'utilisateur à supprimer
     * @param {AccessLayerCUD} callback 
     */
    deleteUser(id, callback) {
        
        var mysql = require('mysql');
            var con = mysql.createConnection({host: "localhost",
            user: "phpmyadmin",
            password: "root",
            database: "bdd"
        })
        
            con.query("DELETE FROM user WHERE id=" + id, (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK");
                }
            });

        con.end();
    }



    /**
     * Requête langages
     */
     /**
     * 
     * @param {AccessLayerGetDatas} callback
     */
    getLangages(callback) {

        var mysql = require('mysql');
            var con = mysql.createConnection({host: "localhost",
            user: "phpmyadmin",
            password: "root",
            database: "bdd"
        })

        con.query("SELECT * FROM langage LIMIT 4", function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
           con.end();

    }

    /**
     * @param {Number} id l'id de l'utilisateur selectionne
     * @param {AccessLayerGetData} callback 
     */

    getLangageById(id, callback) {
       
        var mysql = require('mysql');
        var con = mysql.createConnection({host: "localhost",
        user: "phpmyadmin",
        password: "root",
        database: "bdd"
    })
            con.query("SELECT * FROM langage WHERE id=" + id, 
            function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
            con.end();
    }

    /**
     * @param {any} e données au format json à inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    createLangage(e, callback) {
        
        var mysql = require('mysql');
        var con = mysql.createConnection({host: "localhost",
        user: "phpmyadmin",
        password: "root",
        database: "bdd"
    });

            con.query("INSERT INTO langage(name,src) VALUES('" + e.name + "','" + e.src + "')", (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK CREATE");
                }
            });

        con.end();
    }

    /**
     * @param {any} e données au format json a inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    updateLangage(e, callback) {
    
        var mysql = require('mysql');
            var con = mysql.createConnection({host: "localhost",
            user: "phpmyadmin",
            password: "root",
            database: "bdd"
        })

            this.con.query("UPDATE langage SET name='" + e.name + "',password='" + e.src + "' WHERE id=" + e.id, (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK");
                }
            });
        
        con.end();
    }



    /**
     * Requête exos
     */
     /**
     * 
     * @param {AccessLayerGetDatas} callback
     */
    getExos(callback) {

        var mysql = require('mysql');
            var con = mysql.createConnection({host: "localhost",
            user: "phpmyadmin",
            password: "root",
            database: "bdd"
        })

        con.query("SELECT * FROM exo", function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
           con.end();

    }

    /**
     * @param {Number} id l'id de l'utilisateur selectionne
     * @param {AccessLayerGetData} callback 
     */

    getExoById(id, callback) {
       
        var mysql = require('mysql');
        var con = mysql.createConnection({host: "localhost",
        user: "phpmyadmin",
        password: "root",
        database: "bdd"
    })
       
            con.query("SELECT * FROM exo WHERE id=" + id, 
            function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
            con.end();
    }

     /**
     * @param {Number} id l'id de l'utilisateur selectionne
     * @param {AccessLayerGetData} callback 
     */

    getExoByLangage(id, callback) {
       
        var mysql = require('mysql');
        var con = mysql.createConnection({host: "localhost",
        user: "phpmyadmin",
        password: "root",
        database: "bdd"
    })
       
            con.query("SELECT * FROM exo WHERE id_langage=" + id, 
            function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
            con.end();
    }

    /**
     * @param {any} e données au format json à inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    createExo(e, callback) {
        
        var mysql = require('mysql');
        var con = mysql.createConnection({host: "localhost",
        user: "phpmyadmin",
        password: "root",
        database: "bdd"
    });

            con.query("INSERT INTO exo(name,ennonce,id_langage) VALUES('" + e.name + "','" + e.ennonce + "','" + e.id_langage + "')", (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK CREATE");
                }
            });

        con.end();
    }

    /**
     * @param {any} e données au format json a inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    updateExo(e, callback) {
    
        var mysql = require('mysql');
            var con = mysql.createConnection({host: "localhost",
            user: "phpmyadmin",
            password: "root",
            database: "bdd"
        })

            this.con.query("UPDATE exo SET name='" + e.name + "','" + e.ennonce + "','" + e.id_langage, (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK");
                }
            });
        
        con.end();
    }

    
}