const http = require('http');

const hostname = '127.0.0.1';
const port = 3030;
var mysql = require('mysql');



var express = require('express');
var app = express();
var request = require('request');

// module pour recuperer des arguments dans le corps de la requete
var bodyParser = require('body-parser');

//Gestion des upload de fichiers
var multer = require('multer')

// multipart / form-data
var upload = multer();

// Couche d'accès aux données
const DAO = require('./DAO')
const dao = new DAO();

//Alimentera la propriete body de la requete
app.use(bodyParser.urlencoded({
    extended: true
})); //alimentera la propriete body de la requete
app.use(bodyParser.json());
/**
 * element middleware :
 * cette fonction est appele des qu'une requete est recu
 * l'appel de la fonction next() permet de continuer vers la fonction cible de la requete
 * 
 * Premierement elle assure le CDRS pour les URL du siteweb Et de l'administration . Toutes les autres requetes seront bloquées.
 * pour le siteweb il faut accepeter les requetes GET sur les films et les users, puis  POST 
 * pour l'administration on peut tout accepter
 * 
 * Avec cette configuration on s'assure de la securité de l'api au niveau des accees.
 */
app.use((req, res, next) => {
   
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-Type', 'Application/json');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
})

/**
 * Fin de la partie concernant le middlware
 */




/**
 * Retourne la racine de l'application(permet de s'assurer que le serveur tourne)
 */
app.get("/", (req, res) => {
    res.setHeader("Content-Type", "text/plain");
    res.send("Bienvenue sur l'API")
});

/**
 * Retourne la liste des utilisateurs stockés en base de données
 */
app.get("/users", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getUsers((datas) => {
        res.send(datas);
    });
});

/**
 * Retourne un utilisateur donné
 */
app.get("/user/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getUserById(req.params.id, (datas) => {
        res.send(datas);
    });

});

/**
 * retourne un utilisateur par email
 */
app.get("/user/by/:email", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getUserByEmail(req.params.email, (datas) => {
        res.send(datas);
    });

});

/**
 * créé un utilisateur 
 */
app.post("/users", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.createUser(req.body, (data) => {
        res.send(data);
    });
});

/**
 * Met a jour l'utilisateur passé dans le corps de la requete
 */
app.put("/users", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.updateUser(req.body, (data) => {
        console.log(req.body);
        console.log(data);
        res.send(data+"ok!!!!!");
    });
});

/**
 * Delete un utilisateur dont l'id est passé dans l'URL
 */
app.delete("/user/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.deleteUser(req.params.id, (datas) => {
        res.send(datas);
    });
});



/**
 * partie requête langage
 */
 /**
 * Retourne la liste des langages stockés en base de données
 */
app.get("/langages", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getLangages((datas) => {
        res.send(datas);
    });
});

/**
 * Retourne un langage via l'id
 */
app.get("/langage/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getLangageById(req.params.id, (datas) => {
        res.send(datas);
    });

});

/**
 * ajoute un langage
 */
app.post("/langages", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.createLangage(req.body, (data) => {
        res.send(data);
    });
});

/**
 * met à jour un langage
 */
app.put("/langages", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.updateLangage(req.body, (datas) => {
        res.send(datas);
    });

});



/**
 * partie exos
 */
/**
 * Retourne la liste des exos stockés en base de données
 */
app.get("/exos", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getExos((datas) => {
        res.send(datas);
    });
});

/**
 * Retourne un exo donné
 */
app.get("/exo/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getExoById(req.params.id, (datas) => {
        res.send(datas);
    });

});

/**
 * retourne exo par langage
 */
app.get("/exo/langage/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getExoById(req.params.id, (datas) => {
        res.send(datas);
    });

});

/**
 * créé un exo 
 */
app.post("/exos", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.createExo(req.body, (data) => {
        res.send(data);
    });
});

/**
 * met à jour un exo
 */
app.put("/exos", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.updateExo(req.body, (datas) => {
        res.send(datas);
    });

});



/**
 * partie correction
 */
/**
 * création d'une correction 
 */
app.post("/corrections", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.createCorrection(req.body, (data) => {
        res.send(data);
    });
});

/**
 * met à jour une correction
 */
app.put("/corrections", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.updateCorrection(req.body,(datas) => {
        res.send(datas);
    });

});

/**
 * selectionner tout les exos
 */
app.get("/corrections", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getCorrections((datas)=>{
        res.send(datas);
    });
});

/**
 * retourne une correction par exo
 */
app.get("/correction/exo/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getCorrectionByExo( req.params.id,(datas) => {
        res.send(datas);
    });

});

/**
 * retourne une correction par l'id
 */
app.get("/correction/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getCorrectionById(req.params.id, (datas) => {
        res.send(datas);
    });

});




/**
 * Valeur - test 
 */
/**
 * Retourne la liste des valeurs de correction stockés en base de données
 */
app.get("/valeurtests", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getValeurTest((datas) => {
        res.send(datas);
    });
});

/**
 * Retourne une id valeur de correction donné
 */
app.get("/valeurtest/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.geValeurTestById(req.params.id, (datas) => {
        res.send(datas);
    });

});

/**
 * création d'une correction 
 */
app.post("/valeurtests", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.createCorrection(req.params.valeur, (data) => {
        res.send(data);
    });
});




/**
 * Valeur - correction 
 */
/**
 * Retourne la liste des valeurs de correction stockés en base de données
 */
app.get("/valeurcorrections", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getValeurCorrections((datas) => {
        res.send(datas);
    });
});

/**
 * Retourne une id valeur de correction donné
 */
app.get("/valeurcorrection/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getValeurCorrectionById(req.params.id, (datas) => {
        res.send(datas);
    });

});

/**
 * création d'une correction 
 */
app.post("/valeurcorrections", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.createCorrection(req.params.valeur, (data) => {
        res.send(data);
    });
});



module.exports = app;

app.listen(3030);
